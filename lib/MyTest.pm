package MyTest;

use Mojo::Base 'Mojolicious';

sub startup {
  my $self = shift;
  $self->plugin( OpenAPI => {
    url   => $self->home->rel_file("api.yaml"),
  });
}

1;
