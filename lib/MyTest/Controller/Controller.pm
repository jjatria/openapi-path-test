package MyTest::Controller::Controller;

use Mojo::Base 'Mojolicious::Controller';

sub id {
  my ($self) = @_;
  $self->render( openapi => { target => 'id' } );
}

sub decode {
  my ($self) = @_;
  $self->render( openapi => { target => 'decode' } );
}

1;
